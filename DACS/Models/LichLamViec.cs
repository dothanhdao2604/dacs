﻿using DACS.Models;
using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo.Models
{
    public class LichLamViec
    {
        
        public int Id { get; set; }
        public int? BacSiId { get; set; }
        public DateTime Lich { get; set; }
        public TimeOnly GioBatDau { get; set; }
        public TimeOnly GioKetThuc { get; set; }

        public BacSi? BacSi { get; set; }
        public ICollection<DatLichKham> DatLichKhamBenhs { get; set; }
    }
}
