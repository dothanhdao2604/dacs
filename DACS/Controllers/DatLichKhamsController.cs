﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DACS.Data;
using DACS.Models;

namespace DACS.Controllers
{
    public class DatLichKhamsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DatLichKhamsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: DatLichKhams
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.DatLichKhams.Include(d => d.BenhNhan).Include(d => d.LichLamViec);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: DatLichKhams/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var datLichKham = await _context.DatLichKhams
                .Include(d => d.BenhNhan)
                .Include(d => d.LichLamViec)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (datLichKham == null)
            {
                return NotFound();
            }

            return View(datLichKham);
        }

        // GET: DatLichKhams/Create
        public IActionResult Create()
        {
            ViewData["BenhNhanId"] = new SelectList(_context.BenhNhans, "Id", "Id");
            ViewData["LichLamViecId"] = new SelectList(_context.LichLamViecs, "Id", "Id");
            return View();
        }

        // POST: DatLichKhams/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,BenhNhanId,LichLamViecId,NgayDatLich,TrangThai")] DatLichKham datLichKham)
        {
            if (ModelState.IsValid)
            {
                _context.Add(datLichKham);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BenhNhanId"] = new SelectList(_context.BenhNhans, "Id", "Id", datLichKham.BenhNhanId);
            ViewData["LichLamViecId"] = new SelectList(_context.LichLamViecs, "Id", "Id", datLichKham.LichLamViecId);
            return View(datLichKham);
        }

        // GET: DatLichKhams/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var datLichKham = await _context.DatLichKhams.FindAsync(id);
            if (datLichKham == null)
            {
                return NotFound();
            }
            ViewData["BenhNhanId"] = new SelectList(_context.BenhNhans, "Id", "Id", datLichKham.BenhNhanId);
            ViewData["LichLamViecId"] = new SelectList(_context.LichLamViecs, "Id", "Id", datLichKham.LichLamViecId);
            return View(datLichKham);
        }

        // POST: DatLichKhams/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,BenhNhanId,LichLamViecId,NgayDatLich,TrangThai")] DatLichKham datLichKham)
        {
            if (id != datLichKham.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(datLichKham);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DatLichKhamExists(datLichKham.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BenhNhanId"] = new SelectList(_context.BenhNhans, "Id", "Id", datLichKham.BenhNhanId);
            ViewData["LichLamViecId"] = new SelectList(_context.LichLamViecs, "Id", "Id", datLichKham.LichLamViecId);
            return View(datLichKham);
        }

        // GET: DatLichKhams/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var datLichKham = await _context.DatLichKhams
                .Include(d => d.BenhNhan)
                .Include(d => d.LichLamViec)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (datLichKham == null)
            {
                return NotFound();
            }

            return View(datLichKham);
        }

        // POST: DatLichKhams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var datLichKham = await _context.DatLichKhams.FindAsync(id);
            if (datLichKham != null)
            {
                _context.DatLichKhams.Remove(datLichKham);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DatLichKhamExists(int id)
        {
            return _context.DatLichKhams.Any(e => e.Id == id);
        }
    }
}
