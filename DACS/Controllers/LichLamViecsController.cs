﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DACS.Data;
using DoAnCoSo.Models;

namespace DACS.Controllers
{
    public class LichLamViecsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LichLamViecsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: LichLamViecs
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.LichLamViecs.Include(l => l.BacSi);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: LichLamViecs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lichLamViec = await _context.LichLamViecs
                .Include(l => l.BacSi)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lichLamViec == null)
            {
                return NotFound();
            }

            return View(lichLamViec);
        }

        // GET: LichLamViecs/Create
        public IActionResult Create()
        {
            ViewData["BacSiId"] = new SelectList(_context.BacSis, "Id", "Id");
            return View();
        }

        // POST: LichLamViecs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,BacSiId,Lich,GioBatDau,GioKetThuc")] LichLamViec lichLamViec)
        {
            if (ModelState.IsValid)
            {
                _context.Add(lichLamViec);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BacSiId"] = new SelectList(_context.BacSis, "Id", "Id", lichLamViec.BacSiId);
            return View(lichLamViec);
        }

        // GET: LichLamViecs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lichLamViec = await _context.LichLamViecs.FindAsync(id);
            if (lichLamViec == null)
            {
                return NotFound();
            }
            ViewData["BacSiId"] = new SelectList(_context.BacSis, "Id", "Id", lichLamViec.BacSiId);
            return View(lichLamViec);
        }

        // POST: LichLamViecs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,BacSiId,Lich,GioBatDau,GioKetThuc")] LichLamViec lichLamViec)
        {
            if (id != lichLamViec.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lichLamViec);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LichLamViecExists(lichLamViec.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BacSiId"] = new SelectList(_context.BacSis, "Id", "Id", lichLamViec.BacSiId);
            return View(lichLamViec);
        }

        // GET: LichLamViecs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lichLamViec = await _context.LichLamViecs
                .Include(l => l.BacSi)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lichLamViec == null)
            {
                return NotFound();
            }

            return View(lichLamViec);
        }

        // POST: LichLamViecs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lichLamViec = await _context.LichLamViecs.FindAsync(id);
            if (lichLamViec != null)
            {
                _context.LichLamViecs.Remove(lichLamViec);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LichLamViecExists(int id)
        {
            return _context.LichLamViecs.Any(e => e.Id == id);
        }
    }
}
