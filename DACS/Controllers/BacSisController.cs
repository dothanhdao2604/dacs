﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DACS.Data;
using DACS.Models;

namespace DACS.Controllers
{
    public class BacSisController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BacSisController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: BacSis
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.BacSis.Include(b => b.ChucVu).Include(b => b.Khoa);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: BacSis/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bacSi = await _context.BacSis
                .Include(b => b.ChucVu)
                .Include(b => b.Khoa)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bacSi == null)
            {
                return NotFound();
            }

            return View(bacSi);
        }

        // GET: BacSis/Create
        public IActionResult Create()
        {
            ViewData["ChucVuId"] = new SelectList(_context.ChucVus, "Id", "Id");
            ViewData["KhoaId"] = new SelectList(_context.Khoas, "Id", "Id");
            return View();
        }

        // POST: BacSis/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,GioiTinh,ChucVuId,KhoaId,NgaySinh,DiaChi,SoDienThoai,GioiThieu,KinhNghiem,HocVan,ImageUrl")] BacSi bacSi)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bacSi);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ChucVuId"] = new SelectList(_context.ChucVus, "Id", "Id", bacSi.ChucVuId);
            ViewData["KhoaId"] = new SelectList(_context.Khoas, "Id", "Id", bacSi.KhoaId);
            return View(bacSi);
        }

        // GET: BacSis/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bacSi = await _context.BacSis.FindAsync(id);
            if (bacSi == null)
            {
                return NotFound();
            }
            ViewData["ChucVuId"] = new SelectList(_context.ChucVus, "Id", "Id", bacSi.ChucVuId);
            ViewData["KhoaId"] = new SelectList(_context.Khoas, "Id", "Id", bacSi.KhoaId);
            return View(bacSi);
        }

        // POST: BacSis/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,GioiTinh,ChucVuId,KhoaId,NgaySinh,DiaChi,SoDienThoai,GioiThieu,KinhNghiem,HocVan,ImageUrl")] BacSi bacSi)
        {
            if (id != bacSi.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bacSi);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BacSiExists(bacSi.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ChucVuId"] = new SelectList(_context.ChucVus, "Id", "Id", bacSi.ChucVuId);
            ViewData["KhoaId"] = new SelectList(_context.Khoas, "Id", "Id", bacSi.KhoaId);
            return View(bacSi);
        }

        // GET: BacSis/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bacSi = await _context.BacSis
                .Include(b => b.ChucVu)
                .Include(b => b.Khoa)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bacSi == null)
            {
                return NotFound();
            }

            return View(bacSi);
        }

        // POST: BacSis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bacSi = await _context.BacSis.FindAsync(id);
            if (bacSi != null)
            {
                _context.BacSis.Remove(bacSi);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BacSiExists(int id)
        {
            return _context.BacSis.Any(e => e.Id == id);
        }
    }
}
